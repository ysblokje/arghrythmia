use macroquad::prelude::*;
use macroquad::prelude::Color;
use macroquad::prelude::Texture2D;

use std::{fs, io::Error};
use serde_derive::*;
use hecs::World;

#[derive(Clone)]
#[derive(Deserialize)]
pub struct Position {
    pub x: f32,
    pub y: f32,
}

#[derive(Clone)]
#[derive(Deserialize)]
pub struct Velocity {
    pub x: f32,
    pub y: f32,
}

pub struct Spin {
    pub angular_velocity: f32,
}

#[derive(Clone)]
pub struct Drawable {
    pub drawable_type: DrawableType,
    pub color: Color,
}

pub struct Lightable {
    pub lit: bool,
    pub duration: f32,
    pub counter: f32,
    pub texture1: Texture2D,
    pub texture2: Texture2D,
}

#[derive(Clone)]
pub enum DrawableType {
    Rect { width: f32, height: f32 },
    Circle { radius: f32 },
    Sprite { texture: Texture2D, overlay: Option<Texture2D>, rotation: f32 },
}

#[derive(Clone)]
pub struct ColorTransition {
    pub counter: f32,
    pub time: f32,
    pub from: Color,
    pub to: Color,
}

pub struct BackgroundElement;
pub struct TopElement;
pub struct OverlayElement;

pub struct Collideable {
    pub width: f32,
    pub height: f32,
    pub has_collided: bool,
}

pub struct Particle {
    pub lifetime: f32,
}

pub struct ParticleSpawner {
    pub particle_visuals: Drawable,
    pub color_transition: Option<ColorTransition>,
    pub counter: f32,
    pub interval: f32,
    pub offset: Position,
    pub direction: f32,
    pub spread: f32,
    pub velocity: f32,
    pub lifetime: f32,
}

pub struct Paddle {
    pub max_velocity: f32,
}

pub struct Ball;

pub struct Block {
    pub row: u8,
    pub col: u8,
    pub sturdy: bool,
}

pub struct BallEmitter {
    pub spawn_ball: bool,
    pub delay: f32,
    pub counter: f32,
    pub ball_velocity: Velocity
}

// pub struct Music;

#[derive(Deserialize)]
struct BaseObject {
        position: Position,
        #[serde(default = "default_velocity")]
        velocity: Velocity,
        #[serde(default = "default_sample")]
        sample: Option<u8>,
        #[serde(default = "default_channel")]
        channel: u8,
}


/* fn default_position() -> Position {
    Position { x: 0., y: 0.}
} */

fn default_velocity() -> Velocity {
    Velocity { x: 0.,  y: 0. }
}

/* fn default_string() -> String {
    "".to_string()
} */

fn default_sample() -> Option<u8> { None }

fn default_channel() -> u8 {
    0
}

fn default_launchers() -> Vec<(f32, f32, f32)> {
    return vec![
        (30.0, 230.0, 2.4),
        (440.0, 230.0, -2.4)
    ];
}

fn default_launcher_countdown() -> u8 {
    30
}

#[derive(Deserialize)]
pub struct LevelDescriptor {
    pub name : String,
    pub description : String,
    pub music : String,
    #[serde(default = "default_sounds")]
    pub sounds: Vec<String>,
}

fn default_sounds() -> Vec<String> {
    vec![
        String::from("sound/paddle.ogg"),
        String::from("sound/plop.ogg"),
        String::from("sound/glass-break.ogg"),
    ]
}

#[derive(Deserialize)]
struct BlockFieldObject {
    blocks: Vec<String>
}

#[derive(Deserialize)]
pub struct LauncherObject {
    #[serde(default = "default_launcher_countdown")]
    pub countdown: u8,
    #[serde(default = "default_sample")]
    pub sample: Option<u8>,
    #[serde(default = "default_channel")]
    pub channel: u8,
    #[serde(default = "default_launchers")]
    pub list: Vec<(f32, f32, f32)>,
}

#[derive(Deserialize)]
struct ConfigFile {
    level: LevelDescriptor,
    paddle: BaseObject,
    ball: BaseObject,
    field: BlockFieldObject,
    launchers: LauncherObject,
}

pub struct Level {
    pub leveldescriptor: LevelDescriptor,
    pub launcher_trigger_type: u8,
    pub launcher_trigger: u8,
    pub paddle_trigger_type: u8,
    pub paddle_trigger: u8,
    pub launcher_countdown: u8,
    pub block_rows: u8,
    pub world : World,
}


pub async fn load_level(filename : &str)
                  -> Result<Level, Error> {
    let mut world = World::new();
    let configfilename : String = fs::read_to_string(&filename)?;
    let config: ConfigFile = toml::from_str(&configfilename)?;

    world.spawn((
        Paddle {
            max_velocity: 200.0,
        },
        config.paddle.position,
        config.paddle.velocity,
        Drawable {
            drawable_type: DrawableType::Sprite {
                texture: load_texture("images/paddle.png").await.unwrap(),
                overlay: None,
                rotation: 0.0
            },
            color: WHITE,
        },
        Lightable {
            lit: false,
            duration: 0.05,
            counter: 0.0,
            texture1: load_texture("images/paddle.png").await.unwrap(),
            texture2: load_texture("images/paddle-lit.png").await.unwrap(),
        },
        TopElement,
        Collideable {
            width: 58.0,
            height: 2.0,
            has_collided: false,
        },
    ));

    world.spawn((
        Ball,
        config.ball.position,
        config.ball.velocity,
        Collideable {
            width: 5.0,
            height: 5.0,
            has_collided: false,
        },
        Drawable {
            drawable_type: DrawableType::Sprite {
                texture: load_texture("images/ball.png").await.unwrap(),
                overlay: None,
                rotation: 0.0
            },
            color: WHITE,
        },
        TopElement,
        ParticleSpawner {
            particle_visuals: Drawable {
                drawable_type: DrawableType::Circle { radius: 5.0 },
                color: GRAY,
            },
            color_transition: Some(ColorTransition {
                counter: 0.0,
                time: 0.2,
                from: Color::new(0.165, 1.0, 0.835, 0.66),
                to: Color::new(0.165, 1.0, 0.835, 0.0),
            }),
            counter: 0.0,
            interval: 0.1,
            direction: 0.0,
            spread: 0.0,
            velocity: 0.0,
            offset: Position { x: 5.0, y: 5.0 },
            lifetime: 0.2,
        },
    ));

    // TODO: Load these from a level
    let ball_launchers: Vec<(f32, f32, f32)> = config.launchers.list;
    
    for (pos_x, pos_y, rotation) in ball_launchers { 
        let rotation_to_velocity = Vec2::new(rotation.sin(), -rotation.cos()) * 100.0;

        let ball_velocity = Velocity {
            x: rotation_to_velocity.x,
            y: rotation_to_velocity.y,
        };

        world.spawn((
            BallEmitter {
                spawn_ball: false,
                delay: 1.0,
                counter: 1.0,
                ball_velocity,
            },
            Position {
                x: pos_x,
                y: pos_y
            },
            BackgroundElement,
            Drawable {
                drawable_type: DrawableType::Sprite {
                    texture: load_texture("images/incoming-1.png").await.unwrap(),
                    overlay: None,
                    rotation
                },
                color: WHITE,
            }
        ));
    }

    let mut row_counter = 0;
    for row in config.field.blocks.iter() {
        let mut col_counter = 0;
        for col in row.as_bytes().iter() {
            if *col > '0' as u8 {
                let bcolor = if ['3' as u8, '6' as u8].contains(col) { "red" }
                                else if ['2' as u8, '5' as u8].contains(col) { "yellow" }
                                else { "green" };

                let bpath = format!("{}{}{}", "images/block-", bcolor, "-unlit.png");
                let blpath = format!("{}{}{}", "images/block-", bcolor, ".png");
                let sturdy = ['4' as u8, '5' as u8, '6' as u8].contains(col);
                world.spawn((
                    Block {
                        row: row_counter,
                        col: col_counter,
                        sturdy,
                    },
                    Position {
                        x: col_counter as f32 * 42.0 + 42.,
                        y: row_counter as f32 * 12.0 + 40.,
                    },
                    Drawable {
                        drawable_type: DrawableType::Sprite {
                            texture: load_texture(&bpath)
                                .await
                                .unwrap(),
                            overlay: if sturdy {Some(load_texture("images/block-crystal-overlay.png").await.unwrap())} else { None },
                            rotation: 0.0
                        },
                        color: WHITE,
                    },
                    Lightable {
                        lit: false,
                        // Blocks on the left and right edge stay lit the longest for that sweet UV meter decay effect
                        duration: if col_counter < 5 { (5 - col_counter) as f32 * 0.05 } else { (col_counter - 4) as f32 * 0.05 },
                        counter: 0.0,
                        texture1: load_texture(&bpath).await.unwrap(),
                        texture2: load_texture(&blpath).await.unwrap(),
                    },
                    TopElement,
                    Collideable {
                        width: 38.0,
                        height: 13.0,
                        has_collided: false,
                    },
                ));
            }
            col_counter += 1;
        }
        row_counter += 1;
    }

    world.spawn((
        Position {
            x: 18.0,
            y: 18.0,
        },
        Drawable {
            drawable_type: DrawableType::Rect { width: 450.0, height: 340.0 },
            color: BLACK,
        },
        OverlayElement,
        ColorTransition {
            counter: 0.0,
            time: 1.0,
            from: BLACK,
            to: Color::new(0.0, 0.0, 0.0, 0.0),
        }
    ));

    let mut launcher_trigger_type = 0;
    let mut launcher_trigger = 0;
    let mut paddle_trigger_type = 0;
    let mut paddle_trigger = 1;
    if let Some(sample) = config.launchers.sample {
        launcher_trigger_type = 1;
        launcher_trigger = sample;
    }
    if let Some(sample) = config.paddle.sample {
        paddle_trigger_type = 1;
        paddle_trigger = sample;
    }

    Ok(Level {
        leveldescriptor: config.level,
        launcher_trigger_type,
        launcher_trigger,
        paddle_trigger_type,
        paddle_trigger,
        launcher_countdown: config.launchers.countdown,
        block_rows: row_counter as u8,
        world,
    })
}
