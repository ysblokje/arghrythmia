use crate::*;

pub fn update_input(input: &mut Input, gilrs: &mut Gilrs, gamepadstate: &mut GamepadState, select_prompt: &mut String) {
    while let Some(Event { id, event, time }) = gilrs.next_event() {
        // A small fixed deadzone in case people have jittery or drifty thumbsticks
        let deadzone = 0.1;
        match event {
            EventType::ButtonPressed(Button::DPadRight, _) => { gamepadstate.right = true; }
            EventType::ButtonPressed(Button::DPadLeft, _) => { gamepadstate.left = true; }
            EventType::ButtonPressed(Button::Start, _) => { gamepadstate.start = true; }
            EventType::ButtonPressed(Button::Select, _) => { gamepadstate.select = true; }
            EventType::ButtonReleased(Button::DPadRight, _) => { gamepadstate.right = false; }
            EventType::ButtonReleased(Button::DPadLeft, _) => { gamepadstate.left = false; }
            EventType::ButtonReleased(Button::Start, _) => { gamepadstate.start = false; }
            EventType::ButtonReleased(Button::Select, _) => { gamepadstate.select = false; }
            EventType::AxisChanged(Axis::LeftStickX, val, _ ) => {
                gamepadstate.right = val > deadzone;
                gamepadstate.left = val < -deadzone;
            }
            EventType::Disconnected => { gamepadstate.connected = false; }
            EventType::Connected => { gamepadstate.connected = true; }
            _ => (),
        }
    }
    *select_prompt = String::from(["ENTER", "START"][gamepadstate.connected as usize]);

    input.up = is_key_down(KeyCode::Up) || gamepadstate.up;
    input.down = is_key_down(KeyCode::Down) || gamepadstate.down;
    input.left = is_key_down(KeyCode::Left) || gamepadstate.left;
    input.right = is_key_down(KeyCode::Right) || gamepadstate.right;
    input.select = is_key_pressed(KeyCode::Enter) || gamepadstate.start;
    input.back = is_key_pressed(KeyCode::Escape) || gamepadstate.select;
    input.cheat = is_key_pressed(KeyCode::Delete);
}

pub fn draw_drawables(world: &World) {
    for (_id, (drawable, position)) in world
        .query::<(&Drawable, &Position)>()
        .with::<BackgroundElement>()
        .iter()
    {
        draw_drawable(drawable, position);
    }

    for (_id, (drawable, position)) in world
        .query::<(&Drawable, &Position)>()
        .with::<TopElement>()
        .iter()
    {
        draw_drawable(drawable, position);
    }

    for (_id, (drawable, position)) in world
        .query::<(&Drawable, &Position)>()
        .with::<OverlayElement>()
        .iter()
    {
        draw_drawable(drawable, position);
    }
}

fn draw_drawable(drawable: &Drawable, position: &Position) {
    match drawable.drawable_type {
        DrawableType::Rect { width, height } => {
            draw_rectangle(position.x, position.y, width, height, drawable.color);
        }
        DrawableType::Circle { radius } => {
            draw_circle(position.x, position.y, radius, drawable.color);
        }
        DrawableType::Sprite { texture, overlay, rotation } => {
            draw_texture_ex(
                texture,
                position.x,
                position.y,
                drawable.color,
                DrawTextureParams {
                    dest_size: None,
                    source: None,
                    rotation,
                    flip_x: false,
                    flip_y: false,
                    pivot: None
                }
            );
            if let Some(o) = overlay {
                draw_texture(o, position.x, position.y, WHITE);
            }
        }
    }
}

pub fn transition_colors(world: &mut World, delta: f32) {
    for (_id, (transition, drawable)) in world.query_mut::<(&mut ColorTransition, &mut Drawable)>()
    {
        transition.counter += delta;

        if transition.counter > transition.time {
            transition.counter = transition.time;
        }

        drawable.color.r = transition.from.r * (1.0 - transition.counter / transition.time)
            + transition.to.r * (transition.counter / transition.time);
        drawable.color.g = transition.from.g * (1.0 - transition.counter / transition.time)
            + transition.to.g * (transition.counter / transition.time);
        drawable.color.b = transition.from.b * (1.0 - transition.counter / transition.time)
            + transition.to.b * (transition.counter / transition.time);
        drawable.color.a = transition.from.a * (1.0 - transition.counter / transition.time)
            + transition.to.a * (transition.counter / transition.time);
    }
}

pub fn light_up_paddles(world: &mut World) {
    for (_id, (_paddle, lightable)) in world
        .query_mut::<(&mut Paddle, &mut Lightable)>()
    {
        let Lightable { lit, duration, counter, .. } = lightable;
        *lit = true;
        *counter = *duration;
    }
}

pub fn light_up_blocks(world: &mut World, active_channels: &Vec<u8>, rows: u8, num_channels: u32) {
    for (_id, (block, lightable)) in world
        .query_mut::<(&mut Block, &mut Lightable)>()
    {
        let Lightable { lit, duration, counter, .. } = lightable;
        if active_channels.contains(&((block.row as f32 * (num_channels as f32 / rows as f32)).floor() as u8)) {
            *lit = true;
            *counter = *duration;
        }
    }
}

pub fn handle_lightables(world: &mut World, delta: f32) {
    for (_id, (lightable, drawable)) in world
        .query_mut::<(&mut Lightable, &mut Drawable)>()
    {
        let Lightable { lit, duration: _, counter, texture1, texture2} = lightable;
        if *counter > 0.0 {
            *counter -= delta;
        } else {
            *lit = false;
        }
        if let DrawableType::Sprite { texture, .. } = &mut drawable.drawable_type {
            *texture = [*texture1, *texture2][*lit as usize];
        }
    }
}

pub fn paddle_input(world: &mut World, input: &Input) {
    for (_id, (paddle, velocity)) in world.query_mut::<(&Paddle, &mut Velocity)>() {
        if input.left {
            velocity.x = clamp(velocity.x - 25.0, -paddle.max_velocity, paddle.max_velocity);
        } else if input.right {
            velocity.x = clamp(velocity.x + 25.0, -paddle.max_velocity, paddle.max_velocity);
        } else {
            velocity.x = if velocity.x > 0.0 {
                    clamp(velocity.x - 25.0, 0.0, paddle.max_velocity)
                } else {
                    clamp(velocity.x + 25.0, -paddle.max_velocity, 0.0)
                }
            ;
        }
    }
}

pub fn handle_wall_collisions(world: &mut World, delta: f32) {
    for (_id, (collideable, position, velocity)) in
        world.query_mut::<(&Collideable, &Position, &mut Velocity)>()
    {
        let new_x_pos = position.x + velocity.x * delta;

        if new_x_pos <= 18.0 || new_x_pos + collideable.width >= 482.0 {
            velocity.x *= -1.0;
        }

        let new_y_pos = position.y + velocity.y * delta;

        if new_y_pos <= 18.0 {
            velocity.y *= -1.0;
        }
    }
}

pub fn handle_collisions(world: &mut World, delta: f32, sound_queue: &mut SoundQueue) {
    let mut cmd = CommandBuffer::new();

    for (collider_id, (_ball, collider, position, velocity)) in
        world.query::<(&Ball, &Collideable, &Position, &Velocity)>().iter()
    {
        let new_x_pos = position.x + velocity.x * delta;
        let new_y_pos = position.y + velocity.y * delta;

        let mut new_velocity = velocity.clone();

        for (id, (collideable, collideable_pos)) in
            world.query::<(&Collideable, &Position)>().without::<Paddle>().iter()
        {
            if collider_id != id {
                // Handle horizontal collisions
                let collider_rect =
                    Rect::new(new_x_pos, position.y, collider.width, collider.height);

                let collideable_rect = Rect::new(
                    collideable_pos.x,
                    collideable_pos.y,
                    collideable.width,
                    collideable.height,
                );

                if collider_rect.overlaps(&collideable_rect) {
                    cmd.insert(
                        id,
                        (
                            (),
                            Collideable {
                                has_collided: true,
                                ..*collideable
                            },
                        ),
                    );
                    new_velocity.x *= -1.0;
                    break;
                }

                // Handle vertical collisions
                let collider_rect =
                    Rect::new(position.x, new_y_pos, collider.width, collider.height);

                let collideable_rect = Rect::new(
                    collideable_pos.x,
                    collideable_pos.y,
                    collideable.width,
                    collideable.height,
                );

                if collider_rect.overlaps(&collideable_rect) {
                    cmd.insert(
                        id,
                        (
                            (),
                            Collideable {
                                has_collided: true,
                                ..*collideable
                            },
                        ),
                    );
                    if world.get::<Paddle>(id).is_ok() {
                        sound_queue.push(0);
                    }
                    new_velocity.y *= -1.0;
                    break;
                }
            }
        }


        for (id, (_paddle, collideable, collideable_pos, paddle_velocity)) in
            world.query::<(&Paddle, &Collideable, &Position, &Velocity)>().iter()
        {
            // Handle horizontal collisions
            let collider_rect =
                Rect::new(new_x_pos, position.y, collider.width, collider.height);

            let collideable_rect = Rect::new(
                collideable_pos.x,
                collideable_pos.y,
                collideable.width,
                collideable.height,
            );

            if collider_rect.overlaps(&collideable_rect) {
                cmd.insert(
                    id,
                    (
                        (),
                        Collideable {
                            has_collided: true,
                            ..*collideable
                        },
                    ),
                );
                new_velocity.x *= -1.0;
                break;
            }

            // Handle vertical collisions
            let collider_rect =
                Rect::new(position.x, new_y_pos, collider.width, collider.height);

            let collideable_rect = Rect::new(
                collideable_pos.x,
                collideable_pos.y,
                collideable.width,
                collideable.height,
            );

            if collider_rect.overlaps(&collideable_rect) {
                cmd.insert(
                    id,
                    (
                        (),
                        Collideable {
                            has_collided: true,
                            ..*collideable
                        },
                    ),
                );
                if world.get::<Paddle>(id).is_ok() {
                    sound_queue.push(0);
                }
                new_velocity.y *= -1.0;

                new_velocity.x += paddle_velocity.x / 3.0;

                new_velocity.y += 5.0 * new_velocity.y.signum();
                break;
            }
        }

        cmd.insert(collider_id, ((), new_velocity));
    }

    cmd.run_on(world);
}

pub fn destroy_blocks(world: &mut World, particle_texture: &Texture2D, sound_queue: &mut SoundQueue) {
    let mut cmd = CommandBuffer::new();

    for (block_id, (block, position, collideable, drawable)) in world.query::<(&mut Block, &Position, &mut Collideable, &mut Drawable)>().iter() {
        if collideable.has_collided {
            if block.sturdy {
                if let DrawableType::Sprite { texture: _, overlay, rotation: _ } = &mut drawable.drawable_type {
                    *overlay = None;
                }
                sound_queue.push(2);
                block.sturdy = false;
                collideable.has_collided = false;
                for _ in 0..4 {
                    cmd.spawn((
                        Particle {
                            lifetime: 0.6,
                        },
                        Position {
                            x: position.x + collideable.width / 2.0,
                            y: position.y + collideable.height / 2.0
                        },
                        Velocity {
                            x: rand::gen_range(-60.0, 60.0),
                            y: rand::gen_range(20.0, 50.0),
                        },
                        Spin {
                            angular_velocity: rand::gen_range(-PI, PI),
                        },
                        Drawable {
                            drawable_type: DrawableType::Sprite { texture: *particle_texture, overlay: None, rotation: 0.0 },
                            color: WHITE,
                        },
                        TopElement,
                        ColorTransition {
                            counter: 0.0,
                            time: 0.6,
                            from: WHITE,
                            to: Color::new(1.0, 1.0, 1.0, 0.0),
                        }
                    ));
                }
            } else {
                sound_queue.push(1);
                cmd.despawn(block_id);
            }
        }
    }

    cmd.run_on(world);
}

pub fn move_entities(world: &mut World, delta: f32) {
    for (_id, (position, velocity)) in world.query_mut::<(&mut Position, &Velocity)>() {
        position.x += velocity.x * delta;
        position.y += velocity.y * delta;
    }
}

pub fn spin_entities(world: &mut World, delta: f32) {
    for (_id, (drawable, spin)) in world.query_mut::<(&mut Drawable, &Spin)>() {
        if let DrawableType::Sprite { texture: _, overlay: _, rotation } = &mut drawable.drawable_type {
            *rotation += spin.angular_velocity * delta;
        }
    }
}

pub fn spawn_particles(world: &mut World, delta: f32) {
    let mut cmd = CommandBuffer::new();

    for (_id, (spawner, position)) in world.query_mut::<(&mut ParticleSpawner, &Position)>() {
        spawner.counter += delta;

        while spawner.counter >= spawner.interval {
            spawner.counter -= spawner.interval;

            let direction = rand::gen_range(spawner.direction, spawner.direction + spawner.spread);

            let mut particle_position = position.clone();
            particle_position.x += spawner.offset.x;
            particle_position.y += spawner.offset.y;

            cmd.spawn((
                Particle {
                    lifetime: spawner.lifetime,
                },
                particle_position,
                Velocity {
                    x: direction.sin() * spawner.velocity,
                    y: direction.cos() * spawner.velocity,
                },
                spawner.particle_visuals.clone(),
                if let Some(transition) = &spawner.color_transition {
                    transition.clone()
                } else {
                    ColorTransition {
                        counter: 0.0,
                        time: 1.0,
                        from: spawner.particle_visuals.color,
                        to: spawner.particle_visuals.color,
                    }
                },
                BackgroundElement,
            ));
        }
    }

    cmd.run_on(world);
}

pub fn decrement_launch_counter(context: &mut Context) {
    context.launch_counter -= 1;
}

pub fn check_launch_counter(world: &mut World, context: &mut Context) {
    if context.launch_counter == 0 {
        let launcher_to_activate = rand::gen_range(0, world.query::<((), &BallEmitter)>().iter().count());

        let mut i = 0;

        for (_id, ball_emitter ) in world.query_mut::<&mut BallEmitter>() {
            if i == launcher_to_activate {
                ball_emitter.spawn_ball = true;
                break;
            }

            i += 1;
        }
        context.launch_counter = context.launch_count_max;
    }
}

pub fn spawn_new_balls(world: &mut World, context: &mut Context, delta: f32) {
    let mut cmd = CommandBuffer::new();

    for (_id, (ball_emitter, drawable, position)) in world.query_mut::<(&mut BallEmitter, &mut Drawable, &Position)>() {
        if ball_emitter.spawn_ball {
            drawable.color.a = 1.0;

            ball_emitter.counter -= delta;

            // Cycle through the indicator animation twice before launch
            let fraction = ball_emitter.counter / ball_emitter.delay;
            let timefraction = 1.0 / 6.0;
            let texture_name = if fraction > timefraction * 5.0 { "incoming-1" } else if
                fraction > timefraction * 4.0 { "incoming-2" } else if
                fraction > timefraction * 3.0 { "incoming-3" } else if
                fraction > timefraction * 2.0 { "incoming-1" } else if
                fraction > timefraction * 1.0 { "incoming-2" } else { "incoming-3" };
            if let DrawableType::Sprite { texture, .. } = &mut drawable.drawable_type {
                *texture = *context.misc_textures.get(texture_name).unwrap();
            }

            if ball_emitter.counter <= 0.0 {
                cmd.spawn((
                    Ball,
                    position.clone(),
                    ball_emitter.ball_velocity.clone(),
                    Collideable {
                        width: 5.0,
                        height: 5.0,
                        has_collided: false,
                    },
                    Drawable {
                        drawable_type: DrawableType::Sprite {
                            texture: *context.misc_textures.get("ball").unwrap(),
                            overlay: None,
                            rotation: 0.0
                        },
                        color: WHITE,
                    },
                    TopElement,
                    ParticleSpawner {
                        particle_visuals: Drawable {
                            drawable_type: DrawableType::Circle { radius: 5.0 },
                            color: GRAY,
                        },
                        color_transition: Some(ColorTransition {
                            counter: 0.0,
                            time: 0.2,
                            from: Color::new(0.165, 1.0, 0.835, 0.66),
                            to: Color::new(0.165, 1.0, 0.835, 0.0),
                        }),
                        counter: 0.0,
                        interval: 0.1,
                        direction: 0.0,
                        spread: 0.0,
                        velocity: 0.0,
                        offset: Position { x: 5.0, y: 5.0 },
                        lifetime: 0.2,
                    },
                ));

                ball_emitter.counter = ball_emitter.delay;
                ball_emitter.spawn_ball = false;
            }
        } else {
            drawable.color.a = 0.0;
        }
    }

    cmd.run_on(world);
}

pub fn delete_old_particles(world: &mut World, delta: f32) {
    let mut cmd = CommandBuffer::new();

    for (particle_id, ((), particle)) in world.query_mut::<((), &mut Particle)>() {
        particle.lifetime -= delta;

        if particle.lifetime <= 0.0 {
            cmd.despawn(particle_id);
        }
    }

    cmd.run_on(world);
}

pub fn delete_fallen_balls(world: &mut World) {
    let mut cmd = CommandBuffer::new();

    for (ball_id, (_ball, position)) in world.query::<(&Ball, &Position)>().iter() {
        if position.y >= 360.0 {
            cmd.despawn(ball_id);
        }
    }

    cmd.run_on(world);
}

pub fn check_if_balls_in_play(world: &mut World) -> bool{
    let num_balls = world.query::<((), &Ball)>().iter().count();

    num_balls > 0
}

pub fn check_if_blocks_in_play(world: &mut World) -> bool{
    let num_blocks = world.query::<((), &Block)>().iter().count();

    num_blocks > 0
}
