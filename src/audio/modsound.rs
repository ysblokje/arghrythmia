mod data;
mod handle;
mod sound;

pub use data::*;
pub use handle::*;
pub use sound::*;


#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Command {
    Play,
    Pause,
    Stop,
    Restart,
}
