mod sink;
mod system;

pub use sink::*;
pub use system::*;

#[derive(Clone)]
pub enum AudioEvent {
    MusicNewRow {
        notes: Vec<u8>,
    },
    MusicStart,
    MusicEnd,
    SoundEffectPlayed,
}

impl AudioEvent {
    pub fn new_row(innotes : &Vec<mod_player::Note>) -> crate::AudioEvent {
        let mut outnotes = Vec::<u8>::new();
        for n in innotes {
            outnotes.push(n.get_sample_number());
        }
        AudioEvent::MusicNewRow { notes: outnotes }
    }
}
