pub use std::sync::Arc;

use super::AudioEvent;

pub trait AudioEventSink {
    fn on_event(&self, event: &AudioEvent);
}

pub type SinkStorageType = Arc<dyn AudioEventSink + Send + Sync>;
pub fn eq(a: &SinkStorageType, b: &SinkStorageType) -> bool {
    std::ptr::eq(a.as_ref(), b.as_ref())
}
