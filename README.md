# BEAT NOID

Our submission to the [Linux Game Jam 2022](https://itch.io/jam/linux-game-jam-2022).

An arkanoid-like block breaking game with a musical twist.

[![status-badge](https://ci.codeberg.org/api/badges/RustyGameJammers2022/arghrythmia/status.svg?branch=main)](https://ci.codeberg.org/RustyGameJammers2022/arghrythmia)

## Credits

Design, Code and Graphics by  
**Samsai - Ysblokje - Tuubi**

Music by  
**Gurnu**

Additional levels by  
**Tuubi's wife**


### 3rd Party Assets

Boombox Free (font) © **Jonathan Paterson**  
*https://jonathanpaterson.ca/boom.html*

Sound effects  
[Plop!](https://freesound.org/people/Breviceps/sounds/447910/) by [Breviceps](https://freesound.org/people/Breviceps/) -- License: CC0  
[returnRacquet.wav!](https://freesound.org/people/MAbdurrahman/sounds/361299/) by [MAbdurrahman](https://freesound.org/people/MAbdurrahman/) -- License: CC0  
[Glass Break](https://freesound.org/people/unfa/sounds/221528/) by [unfa](https://freesound.org/people/unfa/) -- License: CC0
